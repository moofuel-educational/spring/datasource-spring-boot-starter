package ru.rtlabs.datasource.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.nio.file.Paths;

/**
 * @author Дмитрий
 * @since 27.07.2017
 */
@ConfigurationProperties(prefix = "com.moofuel.edu.datasource")
public class DataSourceConfigurationProperties {

    private String path = Paths.get(System.getProperty("java.io.tmpdir"), "defaultDataSource.txt").toString();
    private String prefix = "vasya";
    private String postfix = "valera";

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPostfix() {
        return postfix;
    }

    public void setPostfix(String postfix) {
        this.postfix = postfix;
    }

    @Override
    public String toString() {
        return "DataSourceConfigurationProperties{" +
                "path='" + path + '\'' +
                ", prefix='" + prefix + '\'' +
                ", postfix='" + postfix + '\'' +
                '}';
    }
}
