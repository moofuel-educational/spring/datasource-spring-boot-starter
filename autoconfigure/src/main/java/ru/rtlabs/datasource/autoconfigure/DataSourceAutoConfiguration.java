package ru.rtlabs.datasource.autoconfigure;

import com.moofuel.edu.datasource.FileDataSource;
import com.moofuel.edu.datasource.config.FileDataSourceConfiguration;
import com.moofuel.edu.datasource.impl.DefaultFileDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;


/**
 * @author Дмитрий
 * @since 27.07.2017
 */
@Configuration
@ConditionalOnClass(FileDataSource.class)
@EnableConfigurationProperties(DataSourceConfigurationProperties.class)
public class DataSourceAutoConfiguration {

    private final DataSourceConfigurationProperties properties;

    @Autowired
    public DataSourceAutoConfiguration(DataSourceConfigurationProperties properties) {
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean(FileDataSource.class)
    public FileDataSource fileDataSource() throws IOException {
        final FileDataSourceConfiguration configuration = FileDataSourceConfiguration
                .builder()
                .addDataSourcePath(properties.getPath())
                .addPrefix(properties.getPrefix())
                .addPostfix(properties.getPostfix())
                .build();
        return new DefaultFileDataSource(configuration);
    }
}
