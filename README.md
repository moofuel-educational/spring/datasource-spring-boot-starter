<div align="center"><H1>Проект "File Datasource Spring Boot Starter"</H1></div>

## Основные внешние зависимости:
#### 1. Java - 1.8
#### 2. Maven - 3.5.0
---

#### Для использования в вашем проекте стартера необходимо подключить зависимость на модуль <code>starter</code>
~~~
...
    <dependency>
        <groupId>com.moofuel.edu.datasource</groupId>
        <artifactId>starter</artifactId>
        <version>0.1</version>
    </dependency>
...
~~~